# La Boite - Le Blog

La boite - Le Blog est un ensemble de deux applications, [laboite-blog-api](https://gitlab.mim-libre.fr/alphabet/laboite-blog-api) et [laboite-blog-front](https://gitlab.mim-libre.fr/alphabet/laboite-blog-front).

laboite-blog-api: créée avec [Loopback 4](https://loopback.io).
laboite-blog-front: créée avec [Sapper](https://sapper.svelte.dev/) et [Svelte](https://svelte.dev/).

# Getting started

Pour demarrer le projet, il faut cloner les deux applications dans le même dossier.

## Initialisation de l'API

Se placer dans le dossier laboite-blog-api.
Créer un fichier de variables d'environnement : si le fichier `.env` n'existe pas, copier le fichier `.env.example` puis le renommer `.env`.

Dans le fichier `.env`, définir uniquement les variables suivantes et supprimer le reste :

```
MONGO_HOST=localhost
MONGO_PORT=3001
MONGO_DATABASE=meteor
PORT=5000
```

MONGO_HOST correspond à l'URL pour l'accès à la base.
MONGO_PORT correspond au port utilisé par la base.
PORT correspond au port utilisé par l'API.

Note : l'exemple ci-dessus reprend les valeurs par défaut pour une utilisation en local.

### Préparation de l'application front

Se placer dans le dossier laboite-blog-front.
Créer un fichier de variables d'environnement : si le fichier `.env` n'existe pas, copier le fichier `.env.example` puis le renommer `.env`.

Dans le fichier `.env`, définir uniquement les variables suivantes et supprimer le reste:

```
API_HOST="http://localhost:5000"
LABOITE_HOST="http://localhost:3000"
```

API_HOST est l'URL permettant à l'application d'accéder à l'API. Par défaut, le port est 5000 (même port que défini dans le fichier `.env` de laboite-blog-api)
LABOITE_HOST correspond à l'URL utilisée par une application laboite lancée en local.

### Lancement des applications

Pour fonctionner, leblog a besoin impérativement d'une application laboite.
Se rendre dans le dossier laboite, puis lancer le projet de façon habituelle

```
meteor npm start
```

Puis retourner dans le dossier du projet laboite-blog-front :

```bash
cd laboite-blog-front
yarn init-dev
yarn start-dev
```

Utilisant npm-run-all, ces commandes lanceront en parallele les applications client et server respectivement sur les ports 4000 et 5000

## Modifications

Les modifications sur Sapper sont prises instantanément en compte, quant à Loopback, il faut arréter le serveur et le redémarrer avec :

```bash
yarn start-dev
```
